import { YameoPage } from './app.po';

describe('yameo App', () => {
  let page: YameoPage;

  beforeEach(() => {
    page = new YameoPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
