import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { FormsModule } from '@angular/forms'

import { AppComponent } from './app.component';
import { PlaylistsComponent } from './playlists/playlists.component';
import { PlaylistsListComponent } from './playlists/playlists-list.component';
import { PlaylistItemComponent } from './playlists/playlist-item.component';
import { PlaylistDetailsComponent } from './playlists/playlist-details.component';
import { HighlightDirective } from './playlists/highlight.directive';
import { UnlessDirective } from './unless.directive';

import { MusicSearchModule } from './music-search/music-search.module'

import { PlaylistsService } from './playlists/playlists.service'

import {routing} from './app.routing';
import { PlaylistContainerComponent } from './playlists/playlist-container.component';
import { TestingComponent } from './testing.component'

@NgModule({
  declarations: [
    AppComponent,
    PlaylistsComponent,
    PlaylistsListComponent,
    PlaylistItemComponent,
    PlaylistDetailsComponent,
    HighlightDirective,
    UnlessDirective,
    PlaylistContainerComponent,
    TestingComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    //MusicSearchModule,
    routing
  ],
  providers: [PlaylistsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
