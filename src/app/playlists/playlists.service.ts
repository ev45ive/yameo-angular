import { Injectable } from '@angular/core';
import { Playlist } from "app/playlists/playlist";
import { Observable } from 'rxjs/Observable'

@Injectable()
export class PlaylistsService {

  playlists: Playlist[] = [
    { id: '1', name: 'Angular Hits', favourite: false, color: "#00ff00" },
    { id: '2', name: 'Front-end Songs', favourite: false, color: "#0000ff" },
    { id: '3', name: 'The best of Angular', favourite: false, color: "#ff0000" },
  ]

  getPlaylists$() {
    return Observable.of(this.playlists)
  }
  
  getPlaylist$(id) {
    return Observable.of(this.playlists.find(playlist => playlist.id == id))
  }

  constructor() { }

}
