import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { Playlist } from './playlist'

      // <ng-container *ngTemplateOutlet="getTpl(playlist.type); context: $implicit"></ng-container>

      // [style.borderLeftColor]=" ( active == playlist? playlist.color : 'inherit' )"
      // (mouseenter)="active = playlist"
      // (mouseleave)="active = null"
@Component({
  selector: 'playlists-list',
  template: `
<div class="list-group">
  <div class="list-group-item" 

      [highlight]="playlist.color"

      [class.active]="playlist == selected"
      (click)=" select(playlist) "
       *ngFor="let playlist of playlists"> 
       {{playlist.name}}
  </div>
</div>
  `,
  styles: [`
      .list-group-item{
        border-left: 5px solid black;
      }
  ` ]
})
export class PlaylistsListComponent implements OnInit {

  @Input()
  playlists: Playlist[] = [ ]

  select(playlist:Playlist){
    this.selectedChange.emit(playlist)
  }

  @Output('selectedChange')
  selectedChange = new EventEmitter<Playlist>()

  @Input()
  selected:Playlist

  constructor() {
  }

  ngOnInit() {
  }

}
