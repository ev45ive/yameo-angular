import { Component, OnInit } from '@angular/core';
import { Playlist } from "app/playlists/playlist";
import { PlaylistsService } from './playlists.service'
import { Router, ActivatedRoute } from '@angular/router'

@Component({
  selector: 'playlists',
  template: `
  <div class="row">
    <div class="col">
        <playlists-list
              (selectedChange)=" select($event)"
              [selected]="playlist$ | async "
              [playlists]="service.getPlaylists$() | async"></playlists-list>
    </div>
    <div class="col">
      <router-outlet></router-outlet>
    </div>
  </div>
  `,
  styles: [ ]
})
export class PlaylistsComponent implements OnInit {

select(playlist:Playlist){
  this.router.navigate(['playlists',playlist.id])
}

playlist$

  constructor(private router: Router,
              private route: ActivatedRoute,
              public service:PlaylistsService) { 

      this.playlist$ = this.route.firstChild.params
          .pluck('id')
          .switchMap( id => this.service.getPlaylist$(id) )
              
    }

  ngOnInit() {
  }

}
