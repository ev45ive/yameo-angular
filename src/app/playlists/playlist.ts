export interface Playlist {
    id?: string
    name: string;
    favourite: boolean;
    color: string;
}