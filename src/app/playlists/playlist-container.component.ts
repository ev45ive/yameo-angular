import { Component, OnInit } from '@angular/core';

import { PlaylistsService } from './playlists.service'
import { ActivatedRoute } from '@angular/router'
import 'rxjs/add/operator/pluck'

@Component({
  selector: 'playlist-container',
  template: `
    <playlist-details *ngIf="(playlist$ | async) as playlist " [playlist]="playlist"></playlist-details>

    <div *ngIf="!playlist"> Please select playlist </div>
  `,
  styles: []
})
export class PlaylistContainerComponent implements OnInit {

  playlist$

  playlist

  constructor(private route: ActivatedRoute,
              private service:PlaylistsService) { 

      this.playlist$ = this.route.params
          .pluck('id')
          .switchMap( id => this.service.getPlaylist$(id) )

      //this.id = this.route.snapshot.params['id']
      //this.service.getPlaylist$(id)
  }

  ngOnInit() {
  }

}
