import { Component, OnInit, Input,  } from '@angular/core';
import { Playlist } from './playlist'

@Component({
  selector: 'playlist-details',
  template: `
<div [ngSwitch]="mode">
    <div *ngSwitchDefault>
      <p> Name : {{ playlist.name }}</p>
      <p [hidden]="playlist.favourite"> Favourite </p>
      <p [style.color]="playlist.color"> Color </p>
      <button (click)=" edit(playlist) ">Edit</button>
    </div>

    <div *ngSwitchCase=" 'edit' ">
      <div class="form-group">
        <label>Name: </label>
        <input type="text" [(ngModel)]="edited.name" class="form-control">
      </div>
      <div class="form-group">
        <label>Favourite: </label>
        <input type="checkbox" [(ngModel)]="edited.favourite">
      </div>
      <div class="form-group">
        <label>Color: </label>
        <input type="color" [(ngModel)]="edited.color">
      </div>
      <button (click)=" mode = '' ">Cancel</button>
      <button (click)="save()">Save</button>
    </div>
    </div>
  `,
  styles: []
})
export class PlaylistDetailsComponent implements OnInit {

  save(){
    // BEWARE, HERE BE DRAGONS! // 
    Object.assign( this.playlist, this.edited )
    this.mode = ''
  }

  @Input('playlist')
  set setPlaylist(playlist){
    this.playlist = playlist;
    this.mode = ''
  }

  playlist:Playlist

  edited:Playlist

  mode = ''
  edit(playlist){
    this.mode = 'edit'
    this.edited = Object.assign({}, playlist)
  }

  constructor() { }

  ngOnInit() {
  }

}
