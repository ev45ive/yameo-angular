import { Directive, ElementRef, Input, HostListener,HostBinding } from '@angular/core';

@Directive({
  selector: '[highlight]'
})
export class HighlightDirective {

  @Input('highlight')
  set setColor(color){
    this.color = color;
  }
  color;
  
  // <div [style.borderLeftColor]="this.color"
  @HostBinding('style.borderLeftColor')
  get getColor(){
    return this.active? this.color : ''
  }

  active = false;

  @HostListener('mouseenter')
  activate(){
    this.active = true;
  }

  @HostListener('mouseleave')
  deactivate(){
    this.active = false;
  }

  constructor() { 

  }

  ngOnInit(){
  }
}