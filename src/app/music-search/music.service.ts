import { Injectable, Inject, InjectionToken } from '@angular/core';
import { Album } from './album'
import { Http, Response, Headers } from '@angular/http'

export const CLIENT_ID_TOKEN = new InjectionToken('Token for client id injection into music service')

//import 'rxjs/Rx'
import 'rxjs/add/operator/map'
import 'rxjs/add/operator/catch'
import 'rxjs/add/observable/of'
import 'rxjs/add/operator/combineLatest'
import 'rxjs/add/operator/switchMap'
import { Observable, } from 'rxjs/Observable'
import { BehaviorSubject } from 'rxjs/BehaviorSubject'

@Injectable()
export class MusicService {

  getAlbums$(){
    return this.albums$.asObservable()
  }

  albums$ = new BehaviorSubject<Album[]>([])

  search(query = 'batman'){
    let url = `https://api.spotify.com/v1/search?type=album&market=PL&query=${query}`
    
    this.http.get(url,{
      headers:  new Headers({
        'Authorization': `Bearer ${this.getToken()}`
      })
    })
    .map( response => <Album[]>response.json().albums.items )
    .catch( err => {
      this.authorize()
      //throw Error('upsss...')
      return Observable.of([])
    })
    .subscribe( albums => {
      this.albums$.next(albums)
    })
  }

  constructor(
      private http:Http,
      @Inject(CLIENT_ID_TOKEN) client_id:string) {

        this.search()

    let token = this.getToken()
    if(!token){
      this.authorize()
    }
  }

  authorize(){
    localStorage.removeItem('token')
    let client_id= '137294b5c8a84d53922e7e080a2890c8'
    let redirect_uri = 'http://localhost:4200/'

    let url = `https://accounts.spotify.com/authorize?client_id=${client_id}&redirect_uri=${redirect_uri}&response_type=token`;
    window.location.replace(url)
  }

  getToken(){
    let token = localStorage.getItem('token')

    if(!token){
      let match = window.location.hash
                        .match(/#access_token=(.*?)&/)
    
      token = match && match[1]
      if(token)
      localStorage.setItem('token', token)
    }
    return token;
  }

}
