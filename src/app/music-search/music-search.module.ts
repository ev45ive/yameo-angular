import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MusicSearchComponent } from './music-search.component';
import { AlbumsListComponent } from './albums-list.component';
import { SearchFormComponent } from './search-form.component';
import { AlbumComponent } from './album.component';
import { MusicService, CLIENT_ID_TOKEN } from './music.service'
import { HttpModule } from '@angular/http'
import { ReactiveFormsModule } from '@angular/forms';
import { ShortenPipe } from './shorten.pipe'

import {routing} from './music-search.routing'

@NgModule({
  imports: [
    CommonModule,
    HttpModule,
    ReactiveFormsModule,
    routing
  ],
  providers: [
    //{ provide: Http, useClass: myHttp }
    { provide: CLIENT_ID_TOKEN, useValue: '137294b5c8a84d53922e7e080a2890c8'},
    { provide: MusicService, useClass: MusicService }
  ],
  declarations: [
    MusicSearchComponent,
    AlbumsListComponent,
    SearchFormComponent,
    AlbumComponent,
    ShortenPipe
  ],
  exports:[
    //MusicSearchComponent
  ]
})
export class MusicSearchModule {
  constructor(private musicService: MusicService) {
  }
}
