interface Entity{
    id: string;
    name: string;
    href: string;
}

/**
 *  Dokumentacja
 */
export interface Album  extends Entity{
    /**
     * Obrazky
     */
    images: AlbumImage[],
    artists?: Artist[]
}

export interface AlbumImage{
    url: string;
    height: number;
    width: number;
}
export interface Artist extends Entity{}
