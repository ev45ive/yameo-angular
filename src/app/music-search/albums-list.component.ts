import { Component, OnInit } from '@angular/core';
import { MusicService } from './music.service'
import { Album } from './album'

@Component({
  selector: 'albums-list',
  template: `
 <div class="card-group">
    <album class="card"
        *ngFor="let album of albums$ | async "
        [album]="album"></album>
  </div>
  `,
  styles: []
})
export class AlbumsListComponent implements OnInit {

  albums$;

  constructor(private service:MusicService) {
    this.albums$ = this.service.getAlbums$()
      // .map( albums => albums.slice(0,3) )
  }


  // sub;

  ngOnInit() {
    //  this.sub = this.service.getAlbums$()
    // .subscribe( albums =>{
    //   this.albums = albums
    // })
  }

  ngOnDestroy(){
    // this.sub.unsubscribe()
  }

}
