import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'album.card, div.card[album]',
  template: `
    <img class="card-img-top" [src]="album.images[0].url">
    <div class="card-block">
      <h5 class="card-title">{{album.name | shorten:26 }}</h5>
    </div>
  `,
  styles: [`
    :host{
      flex-basis:25% !important;
      max-width:25%;
    }
    :host img{
      width:100%;
    }
  `]
})
export class AlbumComponent implements OnInit {

  @Input()
  album

  constructor() { }

  ngOnInit() {
  }

}
