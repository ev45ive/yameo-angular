import { Component, OnInit } from '@angular/core';
import { MusicService } from './music.service'
import { FormGroup, FormControl, Validators,
    Validator, ValidatorFn, AsyncValidatorFn, AbstractControl, ValidationErrors } from '@angular/forms'

import 'rxjs/add/operator/filter'
import 'rxjs/add/operator/debounceTime'
import 'rxjs/add/operator/distinctUntilChanged'
import 'rxjs/add/operator/combineLatest'
import 'rxjs/add/observable/combineLatest'
import 'rxjs/add/operator/switchMap'
import { Observable } from "rxjs/Observable";

@Component({
  selector: 'search-form',
  template: `
  <form [formGroup]="queryForm">
      <div class="input-group mb-3">
        <input class="form-control" formControlName="query">
        <span class="input-group-btn">
          <button class="btn btn-default">Search</button>
        </span>
      </div>
      <div *ngIf="queryForm.pending"> Please wait ... </div>
      <div *ngIf="queryForm.controls.query.touched  || queryForm.controls.query.dirty ">
        <div *ngIf="queryForm.controls.query.errors?.required">
          Field is required
        </div>
        <div *ngIf="queryForm.controls.query.errors?.minlength">
          Query is too short
        </div>
        <div *ngIf="queryForm.controls.query.errors?.censor">
          this query is not allowed
        </div>
      </div>
    </form>
  `,
  styles: [`
    input.ng-invalid.ng-touched,
    input.ng-invalid.ng-dirty{
      border: 2px solid red !important;
    }
  `]
})
export class SearchFormComponent implements OnInit {

  queryForm:FormGroup

  constructor(private service:MusicService) {

    const censor = (text):ValidatorFn => (control: AbstractControl) => {
      return control.value == text? {
        censor: text
      } : null
    }

    const asyncCensor:AsyncValidatorFn = (control:AbstractControl) => {
      return Observable.create( observer => {
          var error = control.value == 'batman'? {
            censor: 'batman'
          } : null

        setTimeout(()=>{
          observer.next(error)
          observer.complete()
        },2000)
      })
    }

    this.queryForm = new FormGroup({
      'query': new FormControl('',[
        Validators.required,
        Validators.minLength(3),
        //censor('batman')
      ],[
        asyncCensor
      ]),
    })
    console.log(this.queryForm)

    let status$ = this.queryForm.get('query').statusChanges
    let value$ = this.queryForm.get('query').valueChanges
    
    Observable.combineLatest(status$,value$)
    .debounceTime(400)
    //.distinctUntilKeyChanged('1')
    .filter(([status]) => status == "VALID")
    .map( ([,value])=>value)
    .subscribe( (query) => {
      this.service.search(query) 
    })

    // status$.combineLatest(value$,(status,value)=>{
    //   return [status,value];
    // }).filter( ([status]) => status == "VALID")
    // // .map(([,value]) => value)
    // value$.filter( query => query.length >= 3 )
    // .debounceTime(400)
    // .distinctUntilChanged()
    // // .filter( () => this.queryForm.valid )
    // .subscribe( query => this.service.search(query) )
  }

  ngOnInit() {
  }

}
