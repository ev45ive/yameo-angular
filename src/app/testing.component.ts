import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'testing',
  template: `
    <p (click)="message = 'clicked'">{{message}}</p>
  `,
  styles: []
})
export class TestingComponent implements OnInit {

  message = 'Hello!'

  constructor() { }

  ngOnInit() {
  }

}
