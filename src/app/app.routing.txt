import { RouterModule, Routes } from '@angular/router'

const routes:Routes = [

]

const routing = RouterModule.forRoot(routes,{
  enableTracing: true,
  useHash: true
})