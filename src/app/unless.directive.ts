import { Directive, Input, TemplateRef, ViewRef, ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[unless]'
})
export class UnlessDirective {

  cache:ViewRef

  @Input('unless')
  set onHide(hide){
    if(hide){
      this.cache = this.vcr.detach()
    }else{
      if(this.cache){
        this.vcr.insert(this.cache)
      }else{
        this.vcr.createEmbeddedView(this.tpl)
      }
    }
  }

  constructor(private tpl:TemplateRef<any>,
              private vcr: ViewContainerRef) {
                
   }

}
