import { RouterModule, Routes } from '@angular/router'

import { PlaylistsComponent } from './playlists/playlists.component';
import { PlaylistContainerComponent } from './playlists/playlist-container.component';

// import { MusicSearchComponent } from './music-search/music-search.component'

const routes:Routes = [
    { path: '', redirectTo: 'playlists', pathMatch: 'full'},
    // { path: 'music', loadChildren:'./music-search/music-search.module#MusicSearchModule' },
    { path: 'playlists', component: PlaylistsComponent, 
        //resolve: ... tl;dr;
    children:[
        { path: '', component: PlaylistContainerComponent},
        { path: ':id', component: PlaylistContainerComponent}
    ] },
    // { path: 'music', component: MusicSearchComponent },
    { path: '**', redirectTo: 'music' }
]

export const routing = RouterModule.forRoot(routes,{
  enableTracing: true,
  //useHash: true
})