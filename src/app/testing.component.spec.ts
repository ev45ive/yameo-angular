import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import {By} from '@angular/platform-browser'

import { TestingComponent } from './testing.component';

fdescribe('TestingComponent', () => {
  let component: TestingComponent;
  let fixture: ComponentFixture<TestingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports:[],
      providers:[],
      declarations: [ TestingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  xit('should render text', () => {
    expect(fixture.debugElement.query(By.css('p')).nativeElement.innerText).toEqual('testing Works!');
  });

   it('should render message', () => {
     component.message = 'Testing Message'
     fixture.detectChanges()
    expect(fixture.debugElement.query(By.css('p')).nativeElement.innerText).toEqual('Testing Message');
  });

   it('should change message on click', async(() => {
    let p = fixture.debugElement.query(By.css('p'));
//    fixture.autoDetectChanges()
    
    p.triggerEventHandler('click',{})
    fixture.detectChanges()

    expect(p.nativeElement.innerText).toEqual('clicked');
  }));
});
